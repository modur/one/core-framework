# Application
## ImportTrait
* We prepared 'import' trait with functionality to get components from the [container](CONTAINER.md). Choose components which are important to you. The advantage of this is that you can just use this `ImportTrait` instead of using multiple traits and declaring multiple methods to achieved components from container.
* Working in `/src/OctopusCore/App/ImportTrait.php`
    ```
    <?php

    namespace OctopusCore\App;

    // Use required namespaces
    use OctopusCore\Container\ContainerAwareTrait;
    use OctopusCore\Log\ImportLoggerTrait;

    trait ImportTrait
    {
        use ContainerAwareTrait; // import container trait
        use ImportLoggerTrait; // import logger trait

        private Test $test;

        /**
          * Declare this method only once.
          */
        public function test()
        {
            if (empty($this->test)) {
                $this->test = $this->container->get('test');
            }

            return $this->test;
        }
    }
    ```
* Working in `/src/MyApp/Test5.php`
    ```
    <?php

    namespace MyApp;

    // Use only ImportTrait as required namespace
    use OctopusCore\App\ImportTrait;

    class Test5
    {
        use ImportTrait; // import only this trait

        public function something()
        {
            // We can call anything from ImportTrait easily
            $this->test();
            $this->logger->log(LogLevel::INFO, 'SYSTEM-5');
            $router = $this->container->get('router');
        }
    }
    ```
## SpaceShip
* The main purpose of `SpaceShip::class` is to create separate space for configurations files. Thanks this solution you can set just instances which you want to use in the configuration file.
* Just create instance of `SpaceShip::class` and call method `Spaceship::fly()`
* Parameters of `SpaceShip::fly()`:
    * `string $filePath` - path to configuration file (**REQUIRED**)
    * `array $objects = array()` - array of objects defined as asociative (name of the key will be used as name of variable in configuration file and value of the key must be instance of some class)
    * `string $return = null` - you can choose if you need to return result of object from array `$objects` or just `true`. 
        * Format: `'object@method'`
        * For example `$return = 'test@doSomething'` => call object from `$objects` array where name of the key is equal to part before `@` and it's method which is equal to second part after `@` (it means that call is: `$test->doSomething`). Result of the call will be used as final parameter of `$return`.
* Example: working in `/src/MyApp/Test8.php`
    ```
    <?php

    namespace MyApp;

    use OctopusCore\App\ImportTrait;

    class Test8
    {
        use ImportTrait;

        public function setup()
        {
            $file = DIR_CONF . 'test4.php';
            $test = $this->container->get('test6');
            $result = (new SpaceShip)->fly($file, [
                'test6' => $test
            ], 'test6@getResult');

            // In DIR_CONF . 'test4.php' 
            // will be available variable $test6 
            // which will be instance 
            // of container component with name 'test'
            // $test6->getResult() will be returned
            // to variable $result
        }
    }
    ```
## Fallback
* Class Fallback takes care of taking over HTTP error codes. When you create new instance of `Fallback::class` it will automatically load file from `/resources/fallback` directory with same name as the code used in first parameter with extension `.php` (if it is available). 
    * For example: If `/resources/fallback/403.php` is exists then will be loaded with call `new Fallback(403)`. 
* The second parameter of the constructor is array `$inject_objects` which works same as `SpaceShip::class`. It will create own instance of `SpaceShip::class` and pass on this two parameters to method `SpaceShip::fly()`.
## Functions & constants
### Functions
    * At this time there is no functions available. Will be added soon.
### Constants
#### Directories
| NAME       | PARENT   | PATH         |
| ---------- | -------- | ------------ |
| DIR_ROOT   | NONE     | Project root |
| DIR_BIN    | DIR_ROOT | /bin/        |
| DIR_CONF   | DIR_ROOT | /config/     |
| DIR_DOCS   | DIR_ROOT | /docs/       |
| DIR_LANG   | DIR_ROOT | /locales/    |
| DIR_LOG    | DIR_ROOT | /logs/       |
| DIR_PUBLIC | DIR_ROOT | /public/     |
| DIR_RES    | DIR_ROOT | /resources/  |
| DIR_ROUTE  | DIR_ROOT | /routes/     |
| DIR_SRC    | DIR_ROOT | /src/        |
| DIR_TEST   | DIR_ROOT | /tests/      |
| DIR_VENDOR | DIR_ROOT | /vendor/     |
#### Optional
| NAME        | TYPE | DEFAULT | DESCRIPTION |
| ----------- | ---- | ------- | ----------- |
| DISABLE_API | bool | false   | Disable/Enable API |
| ENABLE_DEBUG | bool | false | Disable/Enable Debug |

###### tags: `Modur Framework v1.0.0`
