# Logging
## How it works
* Logging package contains 3 components:
    * **Controller** - Control all defined loggers
    * **Storage** - Store name, settings and other information of Loggers
    * **Builder** - Is passed into the `/config/log.php`. Builder read settings and according to that load loggers. If logger has unknown settings (for example. `'debug: true'` for `FileAdapter` -- it is irelevant), controller will ignore that.
* Logger according to adapter performs activity logging and works on predefined levels.
* Every adapter logs to different storage and under other conditions (file, database, web console, etc...)
* If there is no loggers defined, logger controller automatically define null logger `'main'`. This `'main'` logger logs records, but not save them. 
* At first the logger with name `'main'` must be defined for right functionality of logging!
## Log adapters & configuration
### Available log adapters
* In default there is only `FileAdapter` which logs into the file at `/logs` directory.
    * **Please make sure `/logs` directory exists and is writable!**
    * `FileAdapter` is used to save logs to predefined file
    * Configuration of this adapter is defined by 3 keys and their values in array:
        * `string $loggerName = 'main'` - The name of the logger instance.
        * `string $fileName = date()` - The name of file where logs will be saved. Extension of this file will be always `.log`
        * `array $logLevels = []` - The array of log levels. This array must contains only constants from `LogLevel::class` which is included in PSR-3 package.
* More adapters will be available at Framework Packages extension or you can create your own.
### Managing loggers
* Loggers can be managed in `/config/log.php` configuration file.
* In this configuration file is available log builder which is used to create new logger.
* We can use the template below where we are creating an array `$loggers` and `foreach` loop which create all loggers from the array to the log storage.
* Working in `/config/log.php`
    ```
    <?php

    /** @var LoggerBuilder $log */
    use OctopusCore\Log\Adapter\FileAdapter;
    use OctopusCore\Log\LoggerBuilder;
    use Psr\Log\LogLevel;

    $loggers = [
        'main' => new FileAdapter([ 
            // Use only if you want to use 
            // another name of the key
            //"loggerName" => "main",
                
            "fileName" => "main.log",
                
            // We can´t debug to file
            //"debug" => false,
                
            "logLevels" => [
                LogLevel::INFO,
                LogLevel::NOTICE,
                LogLevel::WARNING
            ]
        ])
    ];

    foreach ($loggers as $logger) {
        $log->setLogger($logger);
    }
    ```
## Error codes & message
* Folder `/resources/log` is used for `.json` configurations of error codes. 
* The name of file must be in format `codes.{poolname}.json`, where `{poolname}` is the name of the logged pool.
* Example:
    * working in `codes.system.json`
    ```
    {
        '431': 'User {u} entered incorrect password'
    }
    ```
* Logger usage:
    * working in `TestClass.php` (any class in your project)
    ```
    use ImportLoggerTrait;

    public function logMe()
    {
        $this->logger->log(
            LogLevel::INFO, // LogLevel
            'SYSTEM_431', // Pool and error code
                
            // {u} will be replaced with admin
            ['u' => 'admin'],
                
            // Log only with logger with name 'main'
            'main'
        )
    }
    ```
* Parameters of `LoggerController:log()` method:
    1. `LogLevel $LogLevel` (eg. LogLevel:ERROR) - logging level from PSR-3 (**REQUIRED**)
    2. `string $code` (eg. 'SYSTEM_431') - error code at pool {poolname}_{errorcode} (**REQUIRED**)
    3. `array $context = []` - array of replacements to error message
    4. `string $logger = '0'` - Name of the logger. If default value, then controller will log with all loggers with selected level
## Debugging
* If `DEBUG_MODE` is defined and setted to value `TRUE` then when you call `$this->logger->log(LogLevel::DEBUG, 'SYSTEM_431')` controller will debug on screen.

###### tags: `Modur Framework v1.0.0`