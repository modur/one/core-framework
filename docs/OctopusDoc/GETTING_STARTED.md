# Getting started
## Requirements
* UNIX like OS with installed:
    * Apache 2.4 or newer with:
        * `mod_ssl` - enabled
        * `mod_rewrite` - enabled
    * PHP 7.4 or newer with:
        * JSON - enabled
* Basic coding skills
## Installation
* Download & extract [ZIP](https://gitlab.com/modur/one/core-framework/-/archive/main/core-framework-main.zip) to your webroot.
**OR**
* Clone GIT repository from [here](https://gitlab.com/modur/one/core-framework.git).
## Request lifecycle
1. Web client send request to server
2. Inside server - creating instance of App class, container with components, router & logging.
3. Loading configurations and other dependencies
4. Start of the App (routing, hadling of request by the component)
5. The component processes the request and return result
6. Web client receive result and process it for end-user
![](https://i.imgur.com/YBLJ7Kh.png)
## Directory structure
* Our framework follows skeleton standard filesystem. See documentation [here](https://github.com/php-pds/skeleton).
* The `/public` & `/docs` folders are the only publicly accessible and visible folders from the URL. 
    * `/public` folder is used for assets of the website (CSS, images, etc...)
    * `/docs` folder is used for documentation files of your source codes.
* All other folders are handled by framework
## Configuration
* All the configurations files are located in `/config` directory.
* At first the framework load `init.php` configuration file. So, we need to create & configure it. We can use template below:
    ```
    <?php

    // Important application namespace
    use OctopusCore\App\App;

    // Include autoload configuration file
    require_once "autoload.php";

    // Create new application instance
    $app = new App();
    ```
## Create your first app (example: IP Manager)
### 1. We have to start with [installation](#Installation).
### 2. Another main step is to do [configuration](#Configuration).
### 3. Create new component
* At first we will create a class `IpManager` in `/src/Service` directory, name of the file will be `IpManager.php` and we need to define `namespace Service`.
    ```
    <?php

    namespace Service;

    class IpManager
    {

    }
    ```
* This namespace is unknown for our autoloader, so it will automatically search in `/vendor/Service`. So, it is mandatory to edit configuration file of autoloder which is located in `/config/autoload.php`.
* In opened configuration file we can find constant named `AUTOLOAD_ALIASES`. It is an array which is used for define all paths to the namespaces. We will add new item `Service` referring to `src/Service` directory. You can place this namespace somewhere else, it is your choice, however for all components is usage of `/src` folder.
    ```
    <?php

    const AUTOLOAD_ALIASES = [
        "OctopusCore" => "src/OctopusCore",
        "Service" => "src/Service"
    ];

    // ...
    ```
### 4. Add component to [container](./CONTAINER.md#Adding-component) (init.php)
* To comfortably work with container, we have to do 2 steps:
    1. #### Implement `ContainerAwareInterrface` to our created `IpManager` component. For the right way of usage of this interface, we will use [`ImportTrait`](./APPLICATION.md#ImportTrait).
    ```
    <?php

    namespace Service;

    use OctopusCore\Container\ContainerAwareInterface;
    use OctopusCore\App\ImportTrait;

    class IpManager implements ContainerAwareInterface
    {
        use ImportTrait;
    }
    ```   
    2. #### Register component in configuration file `/config/init.php`
    ```  
    <?php

    use Service\IpManager;

    // ...

    $app->container()->register(
        IpManager::class,
        'ip-manager'
    );
    ```
* From this moment our component is accessible from container under name `'ip-manager'`. It will automatically load configuration file `/config/ip-manager.php` if exists. We will create this configuration file with the component configuration. For example we can create constant `ALLOW_IP` (array of IP addresses where we can set which one can access to our server).
    ```
    <?php
        
    const ALLOW_IP = [
        '192.168.1.151'
    ];
    ```
### 5. [Logging](./LOGGING.md) of allowed/denied IPs
* To use of logging IP addresses, we have to create new method `IpManager::manage()` in our component `IpManager` which will control accessing to server.
    ```
    // ..
        
    use ImportTrait;
        
    public function manage()
    {
        $client_ip = $_SERVER['REMOTE_ADDR'];
        if (!in_array($client_ip, ALLOW_IP)) {
            // do actions if not allowed
            die('Forbidden');
        } else {
            // do actions if allowed
        }
    }
        
    //..
    ```
* At first we have to create a new [logger](./LOGGING.md#Managing-loggers). Next step is to create JSON configuration file for error messages (directory `/resources/log`). As stated in the logging part of documentation, we will create file `codes.firewall.json`. Content of this file will be:
    ```
    {
        "403": "Access denied for IP {ip}",
        "202": "Access allowed for IP {ip}"
    }
    ```
* And now we can just call method for logging in our component. Logging is now ready!
    ```
    // ..
    
    // Important use statement to use log levels
    use Psr\Log\LogLevel;
    
    // ..
        
    if (!in_array($client_ip, ALLOW_IP)) {
        // do actions if not allowed
        $this->logger()->log(
            LogLevel::NOTICE,
            'FIREWALL_403',
            ['ip' => $client_ip]
        );
        die('Forbidden');
    } else {
        // do actions if allowed
        $this->logger()->log(
            LogLevel::INFO,
            'FIREWALL_202',
            ['ip' => $client_ip]
        );
    }
        
    //..
    ```
### 6. IP addresses filtering
* We are finishing! Now is mandatory to start ourself created method `IpManager::manage()`. We can do it by two ways:
    **Use routing**:
    * Routes are configured in `routes/web.php` file. We will add a new route `'test'` and call our component `IpManager` and it´s method `IpManager::manage()` to control IP address.
    ```
    <?php
        
    /** @var Routes $route */
    use OctopusCore\Container\ContainerExtendedInterface;
    use OctopusCore\Route\Routes;

    $route->add('test', function (
    ContainerExtendedInterface $container
    ) {
        $container->get('ip-manager')->manage(); 
        echo 'Hello World';
    }); 
    ```
    **Or we can control this by area**:
    * We will use `config/start.php` configuration file. It is configuration which will start some of tasks before main start of app and routing.
    ```
    <?php
        
    /** @var Container $start */
    use OctopusCore\Container\Container;
    use Service\IpManager;
        
    /** @var IpManager $ipManager */
    $ipManager = $start->get('ip-manager');
    $ipManager->manage();
    ```
### 7. We can test it now and Hurray! 
* At this moment we can try to go on server, route test (http://localhost/test). If our IP address is in `ALLOW_IP` array from step [4](#4-Add-component-to-container-initphp) then we should see "Hello World!", if not we will see "forbidden". All of this accesses are logged by our configuration to file in `/logs` directory.
* **Please make sure `/logs` directory exists and is writable**
### 8. Now you can creat another one of your choice!
    
###### tags: `Modur Framework v1.0.0`
