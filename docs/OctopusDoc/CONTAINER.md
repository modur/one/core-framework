# Container
## How it works
* Container package contains 3 components:
    * **Container storage:**
        * Store instances & definitions of classes.
    * **Container builder:**
        * Defines user components to container in `/config/init.php` configuration file. These components are used in project as packages.
    * **Main container:**
        * Takes care of component's instances
        * Loading configuration file of the component (see component configuration below)
        * Passes the container to the component 
## Adding component
* Components are added in `/config/init.php` configuration file
* We can use the template below where we are creating an array `$component_bundle` and `foreach` loop which register all components from the array to the container.
    ```
    <?php

    use UserPackage\Test\Example0;
    use UserPackage\Test\Example1 as Example;

    // ...

    $component_bundle = [
        'example-0' => Example0::class,
        'example' => Example::class
    ];

    foreach ($component_bundle as $id => $class) {
        try {
            $app->container()->register($class, $id);
        } catch (ContainerException $e) {
            echo $e->getMessage();
        }
    }
    ```
## Loading component
* Container automatically passes self to classes with `ContainerAwareInterface` implemented.
* We also prepared a `ContainerAwareTrait` with common methods to accessing the container. So You don't need to implement it yourself again, just use this trait.
* Finally the container is accesible from `$this->container` property.
* Getting a component from a container is possible using the `get(id)` method.
    ```
    <?php

    namespace UserPackage\Test;

    // You need to implement this interface
    // to be able to access the container
    class Example0 implements ContainerAwareInterface
    {
        // This trait prepares common methods
        // to accessing the container
        use ContainerAwareTrait;

        public function test()
        {
            // Getting database instance 
            // from container to variable
            $db = $this->container->get('database');
        }
    }
    ```
## Component configuration
* For all components in container is possible to create a config file in `/config` directory with same name as the component and `.php` extension.
**Example:**
Working with `/config/init.php`
    ```
    // ...

    $component_bundle = [
        'database123' => Database::class
    ];

    // ...
    ```
* If we register component named `'database123'` of `Database::class` to the container (as you can see above) then container will automatically load configuration file from `/config/database123.php` if already exists.

###### tags: `Modur Framework v1.0.0`
