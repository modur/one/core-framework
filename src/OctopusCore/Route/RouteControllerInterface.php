<?php

namespace OctopusCore\Route;

/**
 * Interface RouteControllerInterface
 * @package OctopusCore\Route
 */
interface RouteControllerInterface
{
    /**
     * RouteControllerInterface constructor.
     * @param RouterInterface $router
     * @param array $fallback_injects
     */
    public function __construct(
        RouterInterface $router,
        array $fallback_injects = array()
    );

    /**
     * @return void
     */
    public function setRouterFallbackInjects(): void;

    /**
     * @return void
     */
    public function run(): void;
}