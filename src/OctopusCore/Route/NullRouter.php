<?php

namespace OctopusCore\Route;

/**
 * Class NullRouter
 * @package OctopusCore\Route
 */
class NullRouter implements RouterInterface
{
    use NullRouterTrait;
}