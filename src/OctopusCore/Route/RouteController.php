<?php

namespace OctopusCore\Route;

use OctopusCore\App\SpaceShip;
use OctopusCore\App\Fallback;

/**
 * Class RouteController
 * @package OctopusCore\Route
 */
class RouteController implements RouteControllerInterface
{
    use RouteControllerTrait;

    /**
     * @param string $space
     * @param int $position
     */
    public function route(string $space, int $position = 1): void
    {
        $file = DIR_ROUTE . $space . ".php";
        $routes = $this->getRoutes($file);

        if ($routes !== null) {
            $this->router->route($routes, $position);
        } else {
            new Fallback(403, $this->fallbackInjects);
        }
    }

    /**
     * @param string $file
     * @return array|null
     */
    public function getRoutes(string $file): ?array
    {
        if (!file_exists($file)) {
            return null;
        }

        return (new SpaceShip())->fly(
            $file,
            ["route" => new Routes()],
            "route@getRoutes"
        );
    }

    /**
     * @return void
     */
    public function run(): void
    {
        $url = $this->router->getURL(1);

        if ($url !== "api") {
            $this->route("web");
        } else {
            if (!DISABLE_API) {
                header("Content-Type: application/json");
                $this->route("api", 2);
            } else {
                new Fallback(403, $this->fallbackInjects);
            }
        }
    }
}