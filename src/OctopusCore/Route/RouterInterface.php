<?php

namespace OctopusCore\Route;

/**
 * Interface RouterInterface
 * @package OctopusCore\Route
 */
interface RouterInterface
{
    /**
     * RouterInterface constructor.
     */
    public function __construct();

    /**
     * @param $position
     * @return string|null
     */
    public function getUrl($position): ?string;

    /**
     * @param array $fallback_injects
     * @return mixed
     */
    public function setFallbackInjects(array $fallback_injects);

    /**
     * @param array $routes
     * @param int $position
     */
    public function route(array $routes, int $position = 1): void;
}