<?php

namespace OctopusCore\Route;

use OctopusCore\App\Fallback;
use OctopusCore\Container\ContainerAwareInterface;

/**
 * Class Router
 * @package OctopusCore\Route
 */
class Router implements RouterInterface, ContainerAwareInterface
{
    use RouterTrait;

    /**
     * @param array $routes
     * @param int $position
     */
    public function route(array $routes, int $position = 1): void {
        if (!isset($routes[$this->getURL($position)])) {
            new Fallback(404, $this->fallbackInjects);
        } else {
            $routes[$this->getURL($position)]($this->container);
        }
    }
}