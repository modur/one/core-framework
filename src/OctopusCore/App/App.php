<?php

namespace OctopusCore\App;

use OctopusCore\Container\Container;
use OctopusCore\Container\ContainerBuilder;
use OctopusCore\Container\ContainerBuilderAwareInterface;
use OctopusCore\Container\ContainerBuilderAwareTrait;
use OctopusCore\Container\ContainerBuilderInterface;
use OctopusCore\Container\ContainerException;
use OctopusCore\Container\ContainerStorage;
use OctopusCore\Log\Adapter\NullAdapter;
use OctopusCore\Log\LoggerBuilder;
use OctopusCore\Log\LoggerBuilderAwareInterface;
use OctopusCore\Log\LoggerBuilderAwareTrait;
use OctopusCore\Log\LoggerBuilderInterface;
use OctopusCore\Log\LoggerController;
use OctopusCore\Log\LoggerStorage;
use OctopusCore\Route\RouteController;
use OctopusCore\Route\RouterAwareInterface;
use OctopusCore\Route\RouterAwareTrait;
use OctopusCore\Route\Router;

/**
 * Class App
 * @package OctopusCore\App
 */
class App implements
    ContainerBuilderAwareInterface,
    LoggerBuilderAwareInterface,
    RouterAwareInterface
{
    use ContainerBuilderAwareTrait;
    use LoggerBuilderAwareTrait;
    use RouterAwareTrait;
    
    const VERSION = "1.0.0";

    /**
     * @var array $setup
     */
    private array $setup = [];

    /**
     * @var bool $setupRun
     */
    private bool $setupRun = false;

    /**
     * @var bool $setupDelay
     */
    private bool $setupDelay;

    /**
     * App constructor.
     * @param bool $setupDelay
     */
    public function __construct(bool $setupDelay = false)
    {
        $this->setupDelay = $setupDelay;

        /*
         * The container setup must always be in the FIRST order
         * This setup is REQUIRED
         */
        $this->setup[] = "container";
        /*
         * The application setup must always be in the SECOND order
         * This setup is REQUIRED
         */
        $this->setup[] = "app";
        /*
         * The logging setup is REQUIRED
         */
        $this->setup[] = "logging";

        if (!$this->setupDelay) {
            $this->setup();
        }
    }

    /**
     * @return void
     */
    public function setup()
    {
        if (!$this->setupRun) {
            foreach ($this->setup as $setup) {
                $method = "setup" . lcfirst($setup);
                $this->$method();
            }

            $this->setupRun = true;
        }
    }

    /**
     * @return void
     */
    private function setupContainer()
    {
        /*
         * Set containers
         */
        if (!$this->checkContainerStorage()) {
            $this->setContainerStorage(new ContainerStorage());
        }
        if (!$this->checkContainer()) {
            $this->setContainer(new Container($this->containerStorage));
        }
        if (!$this->checkContainerBuilder()) {
            $this->setContainerBuilder(new ContainerBuilder(
                $this->containerStorage
            ));
        }
    }

    /**
     * @throws ContainerException
     */
    private function setupApp()
    {
        /*
         * Register this app instance to container
         */
        $this->containerBuilder->registerObject($this, "app");

        /*
         * Load app configuration
         */
        $config_path = realpath(__DIR__ . "/../../..") . "/config/app.php";
        (new SpaceShip())->fly($config_path);
    }

    /**
     * @throws ContainerException
     */
    private function setupRouting(): void
    {
        /*
         * Validate router and route controller
         */
        $this->validateRouter();
        $this->validateRouteController();
        $this->validateFallbackInjects();

        /*
         * Register router component to container
         */
        $this->containerBuilder->register($this->routerClass, "router");

        /*
         * Set route controller with router injected
         */
        $router = $this->container->get("router");
        $this->routeController = new $this->routeControllerClass(
            $router,
            $this->fallbackInjects
        );
    }

    /**
     * @throws ContainerException
     */
    private function setupLogging()
    {
        /*
         * Set loggers
         */
        if (!$this->checkLoggerStorage()) {
            $this->setLoggerStorage(new LoggerStorage());
        }
        if (!$this->checkLoggerController()) {
            $this->setLoggerController(new LoggerController(
                $this->loggerStorage
            ));
        }
        if (!$this->checkLoggerBuilder()) {
            $this->setLoggerBuilder(new LoggerBuilder($this->loggerStorage));
        }

        /*
         * Load log configuration
         */
        $config_path = realpath(__DIR__ . "/../../..") . "/config/log.php";
        (new SpaceShip())->fly($config_path, ["log" => $this->loggerBuilder]);

        /*
         * Check if main logger is configured
         * If no, register new NullAdapter as main logger
         */
        if (empty($this->loggerStorage->loggers["main"])) {
            $this->loggerBuilder->setLogger(new NullAdapter());
        }

        /*
         * In the end insert log controller to container
         */
        $this->containerBuilder->registerObject(
            $this->loggerController,
            "log"
        );
    }

    /**
     * @return void
     */
    private function validateFallbackInjects()
    {
        foreach ($this->fallbackInjects as $name => $inject) {
            if (!is_string($inject)) {
                die(
                    "Fallback inject with name $name 
                    must be a name of component in container"
                );
            }

            if (!$this->container->has($inject)) {
                die("Fallback inject with name $name not found in container");
            }

            $this->fallbackInjects[$name] = $this->container->get($inject);
        }
    }

    /**
     * @return void
     */
    private function validateRouter()
    {
        if (!$this->checkRouter()) {
            /*
             * Register router component
             */
            $this->setRouter(Router::class);
        }
    }

    /**
     * @return void
     */
    private function validateRouteController()
    {
        if (!$this->checkRouteController()) {
            /*
             * Register route controller component
             */
            $this->setRouteController(RouteController::class);
        }
    }

    /**
     * @throws ContainerException
     */
    private function route()
    {
        $this->setupRouting();

        $this->routeController->run();
    }

    /**
     * @return void
     */
    private function checkSetup(): void
    {
        if (!$this->setupRun) {
            die("The application was not set up");
        }
    }

    /**
     * @return ContainerBuilderInterface
     */
    public function container(): ContainerBuilderInterface
    {
        $this->checkSetup();

        return $this->containerBuilder;
    }

    /**
     * @return LoggerBuilderInterface
     */
    public function log(): LoggerBuilderInterface
    {
        $this->checkSetup();

        return $this->loggerBuilder;
    }

    /**
     * @throws ContainerException
     */
    public function start()
    {
        $this->setup();

        $file = DIR_CONF . "start.php";
        if (file_exists($file)) {
            (new SpaceShip())
                ->fly($file, ["start" => $this->container]);
        }

        $this->route();
    }
}