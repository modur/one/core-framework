<?php

namespace OctopusCore\App;

/**
 * Class SpaceShip
 * @package OctopusCore\App
 */
class SpaceShip
{
    /**
     * @param string $file_path
     * @param array $objects
     * @param string|null $return
     * @return mixed
     */
    public function fly(
        string $file_path,
        array $objects = array(),
        string $return = null
    ) {
        foreach ($objects as $name => $object) {
            ${$name} = $object;
        }

        if (file_exists($file_path)) {
            require_once $file_path;
        }

        if ($return !== null) {
            $return_array = explode("@", $return);
            $object = $return_array[0];
            if (array_key_exists($object, $objects)) {
                $method = $return_array[1];

                return ${$object}->$method();
            }
        }

        return true;
    }
}