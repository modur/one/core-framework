<?php

namespace OctopusCore\Container;

use Psr\Container\ContainerInterface;

/**
 * Interface ContainerExtendedInterface
 * @package OctopusCore\Container
 */
interface ContainerExtendedInterface extends ContainerInterface
{
    /**
     * ContainerExtendedInterface constructor.
     * @param ContainerStorageInterface $storage
     */
    public function __construct(ContainerStorageInterface $storage);
}