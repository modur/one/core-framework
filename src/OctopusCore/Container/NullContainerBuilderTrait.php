<?php

namespace OctopusCore\Container;

/**
 * Trait NullContainerBuilderTrait
 * @package OctopusCore\Container
 */
trait NullContainerBuilderTrait
{
    /**
     * @param string $class
     * @param string $id
     * @throws ContainerException
     */
    public function register(string $class, string $id): void
    {
        throw new ContainerException(
            "You can´t use the container storage with null container",
            7001
        );
    }

    /**
     * @param object $instance
     * @param string $id
     * @throws ContainerException
     */
    public function registerObject(object $instance, string $id): void
    {
        throw new ContainerException(
            "You can´t use the container storage with null container",
            7001
        );
    }
}