<?php

namespace OctopusCore\Container;

/**
 * Interface ContainerBuilderInterface
 * @package OctopusCore\Container
 */
interface ContainerBuilderInterface
{
    /**
     * ContainerBuilderInterface constructor.
     * @param ContainerStorageInterface $storage
     */
    public function __construct(ContainerStorageInterface $storage);

    /**
     * @param string $class
     * @param string $id
     * @throws ContainerException
     */
    public function register(string $class, string $id): void;

    /**
     * @param object $instance
     * @param string $id
     * @throws ContainerException
     */
    public function registerObject(object $instance, string $id): void;
}