<?php

namespace OctopusCore\Container;

/**
 * Trait NullContainerTrait
 * @package OctopusCore\Container
 */
trait NullContainerTrait
{
    /**
     * @param $id
     * @throws ContainerException
     */
    public function get($id)
    {
        throw new ContainerException(
            "You can´t use the container storage with null container",
            7001
        );
    }
}