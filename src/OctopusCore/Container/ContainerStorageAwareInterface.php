<?php

namespace OctopusCore\Container;

/**
 * Interface ContainerStorageAwareInterface
 * @package OctopusCore\Container
 */
interface ContainerStorageAwareInterface
{
    /**
     * ContainerStorageAwareInterface constructor.
     */
    public function __construct();

    /**
     * @param ContainerStorageInterface $storage
     */
    public function setContainerStorage(
        ContainerStorageInterface $storage
    ): void;

    /**
     * @return bool
     */
    public function checkContainerStorage(): bool;
}