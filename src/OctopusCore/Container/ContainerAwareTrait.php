<?php

namespace OctopusCore\Container;

/**
 * Trait ContainerAwareTrait
 * @package OctopusCore\Container
 */
trait ContainerAwareTrait
{
    use ContainerStorageAwareTrait;

    /**
     * @var ContainerExtendedInterface|null $container
     */
    protected ?ContainerExtendedInterface $container = null;

    /**
     * ContainerAwareTrait constructor.
     */
    public function __construct()
    {
        $this->setContainerStorage(new ContainerStorage());
        $this->setContainer(new NullContainer($this->containerStorage));
    }

    /**
     * @param ContainerExtendedInterface $container
     */
    public function setContainer(ContainerExtendedInterface $container): void

    {
        if (!$this->checkContainer()) {
            $this->container = $container;
        } else {
            die("Container is already set");
        }
    }

    /**
     * @return bool
     */
    public function checkContainer(): bool
    {
        if (
            !($this->container instanceof NullContainer)
            && $this->container != null
        ) {
            return true;
        }

        return false;
    }
}