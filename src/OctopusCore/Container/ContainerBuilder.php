<?php

namespace OctopusCore\Container;

/**
 * Class ContainerBuilder
 * @package OctopusCore\Container
 */
class ContainerBuilder implements ContainerBuilderInterface
{
    use ContainerTrait;

    /**
     * @param string $class
     * @param string $id
     * @throws ContainerException
     */
    public function register(string $class, string $id): void
    {
        if (!$this->has($id)) {
            $this->storage->components[$id] = $class;
        } else {
            throw new ContainerException(
                "Component with id $id already registered",
                7002
            );
        }
    }

    /**
     * @param object $instance
     * @param string $id
     * @throws ContainerException
     */
    public function registerObject(object $instance, string $id): void
    {
        if (!$this->has($id)) {
            $this->storage->components[$id] = get_class($instance);
            $this->storage->instances[$id] = $instance;
        } else {
            throw new ContainerException(
                "Component with id $id already registered",
                7002
            );
        }
    }
}