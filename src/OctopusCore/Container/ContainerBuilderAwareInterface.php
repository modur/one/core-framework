<?php

namespace OctopusCore\Container;

/**
 * Interface ContainerBuilderAwareInterface
 * @package OctopusCore\Container
 */
interface ContainerBuilderAwareInterface
{
    /**
     * ContainerBuilderAwareInterface constructor.
     */
    public function __construct();

    /**
     * @param ContainerBuilderInterface $builder
     */
    public function setContainerBuilder(
        ContainerBuilderInterface $builder
    ): void;

    /**
     * @return bool
     */
    public function checkContainerBuilder(): bool;
}