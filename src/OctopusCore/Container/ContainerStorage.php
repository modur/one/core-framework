<?php

namespace OctopusCore\Container;

/**
 * Class ContainerStorage
 * @package OctopusCore\Container
 */
class ContainerStorage implements ContainerStorageInterface
{
    /**
     * @var array $components
     */
    public array $components = [];

    /**
     * @var array $instances
     */
    public array $instances = [];
}