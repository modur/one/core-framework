<?php

namespace OctopusCore\Log;

use Psr\Log\LoggerInterface;

/**
 * Interface LoggerExtendedInterface
 * @package OctopusCore\Log
 */
interface LoggerExtendedInterface extends LoggerInterface
{
    /**
     * LoggerExtendedInterface constructor.
     * @param array $config
     */
    public function __construct(array $config = array());

    /**
     * @return bool
     */
    public function isDebugging();

    /**
     * @return array
     */
    public function getLogLevelRange();

    /**
     * @return string
     */
    public function getLoggerName();

    /**
     * @param $message
     * @param array $context
     * @return string
     */
    public function interpolate($message, array $context = array());
}