<?php

namespace OctopusCore\Log\Adapter;

use OctopusCore\Log\LoggerExtendedInterface;
use OctopusCore\Log\LoggerExtendedTrait;

/**
 * Class NullAdapter
 * @package OctopusCore\Log\Adapter
 */
class NullAdapter implements LoggerExtendedInterface
{
    use LoggerExtendedTrait;

    /**
     * @var string|mixed $loggerName
     */
    private string $loggerName;

    /**
     * @var array|mixed $logLevels
     */
    private array $logLevels;

    /**
     * @var bool|mixed $isDebugging
     */
    private bool $isDebugging;

    /**
     * NullAdapter constructor.
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        $this->loggerName = $config["loggerName"] ?? "main";
        $this->logLevels = $config["logLevels"] ?? [];
        $this->isDebugging = $config["isDebugging"] ?? false;
    }

    /**
     * @return bool|mixed
     */
    public function isDebugging()
    {
        return $this->isDebugging;
    }

    /**
     * @return array|mixed
     */
    public function getLogLevelRange()
    {
        return $this->logLevels;
    }

    /**
     * @return mixed|string
     */
    public function getLoggerName()
    {
        return $this->loggerName;
    }

    /**
     * @param mixed $level
     * @param string $message
     * @param array $context
     */
    public function log($level, $message, array $context = array())
    {
        // noop
    }
}