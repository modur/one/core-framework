<?php

namespace OctopusCore\Log\Adapter;

use Exception;
use OctopusCore\Log\LoggerExtendedInterface;
use OctopusCore\Log\LoggerExtendedTrait;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LogLevel;

/**
 * Class FileAdapter
 * @package OctopusCore\Log\Adapter
 */
class FileAdapter implements LoggerExtendedInterface
{
    use LoggerExtendedTrait;

    /**
     * @var string|mixed $loggerName
     */
    private string $loggerName;

    /**
     * @var string|mixed
     */
    private string $fileName;

    /**
     * @var array|mixed
     */
    private array $logLevels;

    /**
     * FileAdapter constructor.
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        $this->loggerName = $config["loggerName"] ?? "main";
        $this->fileName = $config["fileName"] ?? date("Y-m-d") . ".log";
        $this->logLevels = $config["logLevels"] ?? [];
    }

    /**
     * @return bool
     */
    public function isDebugging(): bool
    {
        return false;
    }

    /**
     * @return array
     */
    public function getLogLevelRange(): array
    {
        return $this->logLevels;
    }

    /**
     * @return mixed|string
     */
    public function getLoggerName()
    {
        return $this->loggerName;
    }

    /**
     * @param string $message
     * @return bool
     */
    public function appendLine(string $message): bool
    {
        try {
            $log_file = fopen(DIR_LOG . $this->fileName, 'a');
            fwrite($log_file, $message . "\r\n");
            fclose($log_file);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param mixed $level
     * @param string $message
     * @param array $context
     */
    public function log($level, $message, array $context = array())
    {
        switch ($level) {
            case LogLevel::EMERGENCY:
            case LogLevel::ALERT:
            case LogLevel::CRITICAL:
            case LogLevel::ERROR:
            case LogLevel::WARNING:
            case LogLevel::NOTICE:
            case LogLevel::INFO:
            case LogLevel::DEBUG:
                $original_code = $context["original-code"] ?? 0;
                $context["original-code"] = null;
                $code_explode = explode('_', $original_code);
                if (count($code_explode) == 1) {
                    $category = null;
                    $code = $original_code;
                } else {
                    $category = strtolower($code_explode[0]);
                    unset($code_explode[0]);
                    $code = strtoupper(implode('_', $code_explode));
                }

                $date = date("c");
                $severity = strtoupper($level);
                $message = trim($this->interpolate($message, $context));
                $this->appendLine(
                    "[$date] $category($code).$severity: $message"
                );
                break;
            default:
                throw new InvalidArgumentException(
                    "Invalid log level $level"
                );
        }
    }
}