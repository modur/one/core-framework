<?php

namespace OctopusCore\Log;

use Exception;
use Psr\Log\LoggerInterface;

/**
 * Trait NullLoggerBuilderTrait
 * @package OctopusCore\Log
 */
trait NullLoggerBuilderTrait
{
    /**
     * @param LoggerInterface $logger
     * @throws Exception
     */
    public function setLogger(LoggerInterface $logger)
    {
        throw new Exception(
            "You can´t use the logger storage with null container",
            7033
        );
    }
}