<?php

namespace OctopusCore\Log;

/**
 * Trait LoggerBuilderAwareTrait
 * @package OctopusCore\Log
 */
trait LoggerBuilderAwareTrait
{
    use LoggerControllerAwareTrait;

    /**
     * @var LoggerBuilderInterface|null $loggerBuilder
     */
    protected ?LoggerBuilderInterface $loggerBuilder = null;

    public function __construct()
    {
        $this->setLoggerStorage(new LoggerStorage());
        $this->setLoggerController(new NullLoggerController($this->loggerStorage));
        $this->setLoggerBuilder(new NullLoggerBuilder($this->loggerStorage));
    }

    /**
     * @param LoggerBuilderInterface $builder
     */
    public function setLoggerBuilder(
        LoggerBuilderInterface $builder
    ): void {
        if (!$this->checkLoggerBuilder()) {
            $this->loggerBuilder = $builder;
        } else {
            die("Logger builder is already set");
        }
    }

    /**
     * @return bool
     */
    public function checkLoggerBuilder(): bool
    {
        if (
            !($this->loggerBuilder instanceof NullLoggerBuilder)
            && $this->loggerBuilder != null
        ) {
            return true;
        }

        return false;
    }
}