<?php

namespace OctopusCore\Log;

use Psr\Log\LoggerAwareInterface;

/**
 * Interface LoggerBuilderInterface
 * @package OctopusCore\Log
 */
interface LoggerBuilderInterface extends LoggerAwareInterface
{
    /**
     * LoggerBuilderInterface constructor.
     * @param LoggerStorageInterface $storage
     */
    public function __construct(LoggerStorageInterface $storage);
}