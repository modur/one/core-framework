<?php

namespace OctopusCore\Log;

/**
 * Trait LoggerStorageTrait
 * @package OctopusCore\Log
 */
trait LoggerStorageTrait
{
    /**
     * @var LoggerStorageInterface $storage
     */
    private LoggerStorageInterface $storage;

    /**
     * LoggerStorageTrait constructor.
     * @param LoggerStorageInterface $storage
     */
    public function __construct(LoggerStorageInterface $storage)
    {
        $this->storage = $storage;
    }
}