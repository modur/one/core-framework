<?php

namespace OctopusCore\Log;

use Psr\Log\LogLevel;

/**
 * Class LoggerController
 * @package OctopusCore\Log
 */
class LoggerController implements LoggerControllerInterface
{
    use LoggerControllerTrait;

    /**
     * @var array|null $codes
     */
    private ?array $codes = null;

    /**
     * @return array|null
     */
    private function loadCodeMessages()
    {
        if (empty($this->codes)) {
            $base_path = realpath(__DIR__ . '/../../..') . "/resources/log/codes.";
            $files = glob($base_path . "*.json");
            foreach ($files as $file) {
                $codes = json_decode(file_get_contents($file), true);
                if (!empty($codes)) {
                    $file_name = array_filter(explode($base_path, $file))[1];
                    $file_name = array_filter(explode(".json", $file_name))[0];
                    foreach ($codes as $code => $message) {
                        $this->codes[strtoupper($file_name) . "_" . $code]
                            = $message;
                    }

                    unset($codes);
                }
            }
        }

        return $this->codes;
    }

    /**
     * @param string $code
     * @return mixed|string
     */
    private function getCodeMessage(string $code)
    {
        switch ($code) {
            case "0":
                $message = 'Wrong log attempt with level {level}';
                break;
            case "1":
                $message = 'Wrong log attempt with code {code}';
                break;
            default:
                $message = $this->loadCodeMessages()[$code] ?? null;
        }

        return $message;
    }

    /**
     * @param $level
     * @param string $code
     * @param array $context
     * @param string $logger
     * @return void
     */
    public function log(
        $level,
        string $code,
        array $context = array(),
        string $logger = '0'
    ) {
        if (
            $level !== LogLevel::DEBUG
            || (defined("ENABLE_DEBUG") && ENABLE_DEBUG)
        ) {
            switch ($level) {
                case LogLevel::EMERGENCY:
                case LogLevel::ALERT:
                case LogLevel::CRITICAL:
                case LogLevel::ERROR:
                case LogLevel::WARNING:
                case LogLevel::NOTICE:
                case LogLevel::INFO:
                case LogLevel::DEBUG:
                    $message = $this->getCodeMessage($code);
                    if (!empty($message)) {
                        $context["original-code"] = $code;
                        $message = "{original-code} " . $message;
                        if ($logger == '0') {
                            foreach ($this->storage->loggersMap[$level] as $logger) {
                                $this->storage->loggers[$logger]->$level(
                                    $message,
                                    $context
                                );
                            }
                        } elseif (!empty($this->storage->loggers[$logger])) {
                            $this->storage->loggers[$logger]->$level(
                                $message,
                                $context
                            );
                        }
                    } else {
                        $this->log(LogLevel::INFO, "1", ["code" => $code]);
                    }
                    break;
                default:
                    $this->log(LogLevel::INFO, "0", ["level" => $level]);
            }
        }
    }
}