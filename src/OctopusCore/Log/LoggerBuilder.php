<?php

namespace OctopusCore\Log;

use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Class LoggerBuilder
 * @package OctopusCore\Log
 */
class LoggerBuilder implements LoggerBuilderInterface
{
    use LoggerStorageTrait;

    /**
     * @param LoggerInterface $logger
     * @throws Exception
     */
    public function setLogger(LoggerInterface $logger)
    {
        if ($logger instanceof LoggerExtendedInterface) {
            $logger_name = $logger->getLoggerName();
            if (!array_key_exists($logger_name, $this->storage->loggers)) {
                $this->storage->loggers[$logger_name] = $logger;

                if ($logger->isDebugging()) {
                    $this->storage->loggers[LogLevel::DEBUG][] = $logger_name;
                }

                foreach ($logger->getLogLevelRange() as $logLevel) {
                    switch ($logLevel) {
                        case LogLevel::EMERGENCY:
                        case LogLevel::ALERT:
                        case LogLevel::CRITICAL:
                        case LogLevel::ERROR:
                        case LogLevel::WARNING:
                        case LogLevel::NOTICE:
                        case LogLevel::INFO:
                            $this->storage->loggersMap[$logLevel][]
                                = $logger_name;
                            break;
                        default:
                            throw new Exception(
                                "Invalid log level in $logger_name logger range",
                                7032
                            );
                    }
                }
            } else {
                throw new Exception(
                    "Logger with name $logger_name is already registered.",
                    7031
                );
            }
        } else {
            throw new Exception(
                "Logger instance must implement LoggerExtendedInterface",
                7030
            );
        }
    }
}