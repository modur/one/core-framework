<?php

namespace OctopusCore\Log;

/**
 * Class NullLoggerController
 * @package OctopusCore\Log
 */
class NullLoggerController implements LoggerControllerInterface
{
    use LoggerControllerTrait;
    use NullLoggerControllerTrait;
}