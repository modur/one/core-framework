<?php

namespace OctopusCore\Log;

/**
 * Trait LoggerControllerAwareTrait
 * @package OctopusCore\Log
 */
trait LoggerControllerAwareTrait
{
    use LoggerStorageAwareTrait;

    /**
     * @var LoggerControllerInterface|null $loggerController
     */
    protected ?LoggerControllerInterface $loggerController = null;

    /**
     * LoggerControllerAwareTrait constructor.
     */
    public function __construct()
    {
        $this->setLoggerStorage(new LoggerStorage());
        $this->setLoggerController(new NullLoggerController(
            $this->loggerStorage
        ));
    }

    /**
     * @param LoggerControllerInterface $loggerController
     */
    public function setLoggerController(
        LoggerControllerInterface $loggerController
    ): void {
        if (!$this->checkLoggerController()) {
            $this->loggerController = $loggerController;
        } else {
            die("Logger controller is already set");
        }
    }

    /**
     * @return bool
     */
    public function checkLoggerController(): bool

    {
        if (
            !($this->loggerController instanceof NullLoggerController)
            && $this->loggerController != null
        ) {
            return true;
        }

        return false;
    }
}