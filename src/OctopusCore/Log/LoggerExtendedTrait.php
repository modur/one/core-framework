<?php

namespace OctopusCore\Log;

use Psr\Log\LoggerTrait;

/**
 * Trait LoggerExtendedTrait
 * @package OctopusCore\Log
 */
trait LoggerExtendedTrait
{
    use LoggerTrait;

    /**
     * @param $message
     * @param array $context
     * @return string
     */
    public function interpolate($message, array $context = array())
    {
        $replace = array();
        foreach ($context as $key => $val) {
            $replace['{' . $key . '}'] = $val;
        }

        return strtr($message, $replace);
    }
}