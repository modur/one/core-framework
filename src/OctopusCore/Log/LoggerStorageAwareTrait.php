<?php

namespace OctopusCore\Log;

/**
 * Trait LoggerStorageAwareTrait
 * @package OctopusCore\Log
 */
trait LoggerStorageAwareTrait
{
    /**
     * @var LoggerStorageInterface $loggerStorage
     */
    public LoggerStorageInterface $loggerStorage;

    /**
     * LoggerStorageAwareTrait constructor.
     */
    public function __construct()
    {
        $this->setLoggerStorage(new LoggerStorage());
    }

    /**
     * @param LoggerStorageInterface $storage
     */
    public function setLoggerStorage(LoggerStorageInterface $storage): void
    {
        if (!$this->checkLoggerStorage()) {
            $this->loggerStorage = $storage;
        } else {
            die("Logger storage is already set");
        }
    }

    /**
     * @return bool
     */
    public function checkLoggerStorage(): bool
    {
        if (!isset($this->loggerStorage)) {
            return false;
        }

        return true;
    }
}