# Changelog
## Version 1.0.0
> Initial release
### Features:
* PSR-11 Container
* PSR-4 Autoload
* PSR-3 Logger
* Simple routing (API supported)
* PSR-1 & PSR-12 Coding Styles
* PHP 8 ready, min. PHP 7.4
* SOLID principles
* Skeleton directory structure
* NON-static app
* ANTI-singleton app

###### tags: `Modur Framework v1.0.0`
